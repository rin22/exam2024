valid_inputs = ["+", "-", "*", "/", "=", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

operators = ["+", "-", "/", "*"]

input_list = []

while True:
  user_input = input("Zadejte čísla, operátory nebo rovná se:")
  if user_input in valid_inputs:
    if user_input != "=":
      if user_input in operators and input_list and input_list[-1] in operators:
        input_list.pop()
        input_list.append(str(user_input))
      else: input_list.append(str(user_input))
    else:
      if input_list[0] in operators:
        input_list.pop(0)
      print(str(''.join(input_list)) + "=" + str(eval(''.join(input_list))))
      break
    